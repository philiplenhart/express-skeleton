// enables ES6 ('import'.. etc) in Node
require('babel-core/register')
require('babel-polyfill')

//Require the dev-dependencies
import chai from 'chai'
import chaiHttp from 'chai-http'

const should = chai.should()

import server from '../app'

chai.use(chaiHttp)

describe('Test the system', () => {
    describe('Retrieve the API endpoints', () => {
        it('it should GET the landing page', (done) => {
            chai.request(server)
                .get('/')
                .auth('admin', 'supersecret')
                .end((err, res) => {
                    should.exist(res)
                    res.should.have.status(200)
                    done()
                })
        })
        it('it should GET a favicon', (done) => {
            chai.request(server)
                .get('/favicon.ico')
                .auth('admin', 'supersecret')
                .end((err, res) => {
                    should.exist(res)
                    res.should.have.status(200)
                    done()
                })
        })
        it('it should NOT find this non existing page', (done) => {
            chai.request(server)
                .get('/not-found-route')
                .auth('admin', 'supersecret')
                .end((err, res) => {
                    should.exist(res)
                    res.should.have.status(404)
                    done()
                })
        })
        it('it should NOT be authenticated for this page', (done) => {
            chai.request(server)
                .get('/not-found-route')
                .auth('admin', 'wrongsecret')
                .end((err, res) => {
                    should.exist(res)
                    res.should.have.status(401)
                    done()
                })
        })
    })
})
