# Basic Express.js skeleton
  
This is a basic skeleton to start your project. It uses Yarn instead of npm, but npm can also be used.

## Installation

Installation of the required modules is done using the
[`yarn` command](https://yarnpkg.com/en/docs/usage):

```bash
$ yarn
```

After the installation you can start your server with
```bash
$ yarn run start
```

Mocha is used for testing:
```bash
$ yarn run test
```

ESlint is included and can be executed with
```bash
$ yarn run lint
```

To run linting and testing together you can use
```bash
$ yarn run test:all
```

## License

   The MIT License (MIT)
   
   Copyright (c) 2018 Philip Lenhart <dev@philiplenhart.de>
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.